import mockAxios from 'axios';
import { getCampaigns, getCampaign } from '../../services/campaignApi';

describe('Platform Tab Container', () => {
  const campaigns = [{id: 1, name: 'Test 1'}, {id: 2, name: 'Test 2'}, {id: 3, name: 'Test 3'}];

  it('getCampaigns returns all campaigns', async () => {
    const result = { data: { data: { campaigns }}};
    const resolved = new Promise((resolve) => resolve(result));
    mockAxios.post.mockImplementation(() => resolved);
    const response = await getCampaigns();
    expect(response).toBe(campaigns);
  });

  it('getCampaign returns campaign with matching id', async () => {
    const campaign = [campaigns[1]];
    const result = { data: { data: { campaigns: campaign }}};
    const resolved = new Promise((resolve) => resolve(result));
    mockAxios.post.mockImplementation(() => resolved);
    const response = await getCampaign(2);
    expect(response).toBe(campaign);
  });
});
