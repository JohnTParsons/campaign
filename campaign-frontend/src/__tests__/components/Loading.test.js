import React from 'react';
import { shallow } from 'enzyme';
import Loading from '../../components/Loading';

describe('Loading component', () => {
  it('returns HTML', () => {
    const wrapper = shallow(<Loading />);
    expect(wrapper.contains(<div><h1>Loading</h1></div>)).toBe(true);
  });
});
