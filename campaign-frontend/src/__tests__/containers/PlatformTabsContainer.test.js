import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';
import configureStore from 'redux-mock-store';
import PlatformTabsContainer from '../../containers/PlatformTabsContainer';

describe('Platform Tab Container', () => {
  const allPlatforms = { facebook: {}, instagram: {}, google: {} };

  const render = (platforms, dispatch) => {
    const state = {
      campaign: { records: [{ platforms }] }
    };

    const store = configureStore()(state);
    if (dispatch) store.dispatch = dispatch;
    return mount(
      <Provider store={store}>
        <PlatformTabsContainer />
      </Provider>
    );
  };

  it('renders no tabs when no platforms in response', () => {
    const wrapper = render(null, null);
    expect(wrapper.find(PlatformTabsContainer).exists()).toBe(true);
    expect(wrapper.find('Tab').length).toBe(0);
  });

  it('renders all tabs when all platforms in response', () => {
    const wrapper = render(allPlatforms, null);
    expect(wrapper.find(PlatformTabsContainer).exists()).toBe(true);
    expect(wrapper.find('Tab').length).toBe(3);
  });

  it('dispatches action when onPlatformSelection called', () => {
    const dispatch = jest.fn();
    const wrapper = render(allPlatforms, dispatch);
    const component = wrapper.find(PlatformTabsContainer).find('PlatformTabsContainer');
    expect(component.prop('campaign')).toEqual({ platforms: allPlatforms });
    expect(component.prop('onPlatformSelection')).toEqual(expect.any(Function));
    component.props().onPlatformSelection('facebook');
    expect(dispatch).toHaveBeenCalledTimes(1);
  });
});
