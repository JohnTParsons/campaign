import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

import {
  selectPlatform
} from '../../store/actions';
import {
  campaignRecord
} from '../../store/selectors/campaign.selectors';

const PLATFORMS = ['facebook', 'instagram', 'google'];

class PlatformTabsContainer extends Component {

  constructor(props) {
    super(props);
    this.state = {
      tabIndex: 0,
    };
  }

  handleChange = (index) => {
    const platformName = PLATFORMS[index];
    this.setState({
      tabIndex: index
    });
    this.props.onPlatformSelection(platformName);
  };

  render() {
    const { platforms } = this.props.campaign || {};
    if (!platforms) {
      return null;
    }

    return (
      <div>
        <Tabs value={this.state.tabIndex} onChange={(event, index) => this.handleChange(index)} >
          {platforms.facebook &&
            <Tab label="facebook" value={0}>
              <div>Facebook</div>
            </Tab>
          }

          {platforms.instagram &&
            <Tab label="instagram" value={1}>
              <div>Instagram</div>
            </Tab>
          }

          {platforms.google &&
            <Tab label="google" value={2}>
              <div>Google</div>
            </Tab>
          }
        </Tabs>
        <br />
      </div>
    )
  }
}

PlatformTabsContainer.propTypes = {
  campaign: PropTypes.object
};

function mapStateToProps(state) {

  return {
    campaign: campaignRecord(state)
  }
}

function mapDispatchToProps(dispatch) {

  return {
    onPlatformSelection: (platformName) => dispatch(selectPlatform(platformName))
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(PlatformTabsContainer);
