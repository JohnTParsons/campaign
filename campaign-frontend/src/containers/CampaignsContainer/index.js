import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import CampaignsGrid from '../../components/CampaignsGrid';
import Loading from '../../components/Loading';
import ScrollToTop from '../../components/ScrollToTop';

import {
    refreshCampaigns,
    campaignsError,
    doPauseCampaignsPulling,
    doResumeCampaignsPulling,
    selectCampaign
} from '../../store/actions';
import {
    isFetching,
    lastUpdated,
    errorMessage,
    pausePulling,
    campaignRecordsRaw,
    selectedCampaign
} from '../../store/selectors/campaigns.selectors';

class CampaignsContainer extends PureComponent {

    handleErrorCloseClick = e => {
        e.preventDefault();
        this.props.onCampaignsError();
    };

    handleRefreshClick = e => {
        e.preventDefault();
        this.props.onRefreshCampaigns();
    };

    handlePauseCampaignsPullingClick = e => {
        e.preventDefault();
        this.props.pausePulling ? this.props.doResumeCampaignsPulling() : this.props.doPauseCampaignsPulling();
    };

    render() {
        const {
            isFetching, 
            campaignRecords,
            lastUpdated, 
            errorMessage,
            pausePulling,
            onCampaignSelection,
            selectedCampaign
        } = this.props;

        return (
            <div>
                <ScrollToTop />
                {errorMessage !== null &&
                    <p className="error">
                        {errorMessage}&nbsp;
                        <button onClick={this.handleErrorCloseClick}>X</button>
                    </p>
                }
                <p>
                    {lastUpdated !== null  &&
                        <span>
                            Last updated at {new Date(lastUpdated).toLocaleTimeString()}.
                            {' '}
                        </span>
                    }
                    {!isFetching &&
                        <button
                            onClick={this.handleRefreshClick}>
                            Refresh
                        </button>
                    }
                </p>
                {isFetching && campaignRecords.length === 0 &&
                    <Loading />
                }
                {!isFetching && campaignRecords.length === 0 &&
                    <h3>Empty</h3>
                }
                {campaignRecords.length > 0 && onCampaignSelection &&
                    <div style={{ opacity: isFetching ? 0.5 : 1 }}>
                        <CampaignsGrid records={campaignRecords} onCampaignSelection={onCampaignSelection} selectedCampaign={selectedCampaign} />
                        <div>
                            {pausePulling ? 
                                (
                                    <button onClick={this.handlePauseCampaignsPullingClick}>Resume campaigns pulling</button>
                                ) : 
                                (
                                    <button onClick={this.handlePauseCampaignsPullingClick}>Pause campaigns pulling</button>
                                )
                            }
                        </div>
                    </div>                    
                }
            </div>            
        );
    }
}

CampaignsContainer.propTypes = {
    campaignRecords: PropTypes.array,
    isFetching: PropTypes.bool,
    lastUpdated: PropTypes.number,
    errorMessage: PropTypes.string,
    pausePulling: PropTypes.bool
};

function mapStateToProps(state) {

    return {
        campaignRecords: campaignRecordsRaw(state),
        isFetching: isFetching(state),
        lastUpdated: lastUpdated(state),
        errorMessage: errorMessage(state),
        pausePulling: pausePulling(state),
        selectedCampaign: selectedCampaign(state)
    }
}

function mapDispatchToProps(dispatch) {

    return {
        onCampaignSelection: (campaignSelection) => dispatch(selectCampaign(campaignSelection)),

        onCampaignsError: () => dispatch(campaignsError()),

        onRefreshCampaigns: () => dispatch(refreshCampaigns()),

        doResumeCampaignsPulling: () => dispatch(doResumeCampaignsPulling()),

        doPauseCampaignsPulling: () => dispatch(doPauseCampaignsPulling())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CampaignsContainer);
