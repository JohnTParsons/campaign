import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Loading from '../../components/Loading';
import PlatformTabsContainer from '../../containers/PlatformTabsContainer';
import CampaignPlatformInsights from '../../components/CampaignPlatformInsights';
import CampaignPlatformOther from '../../components/CampaignPlatformOther';

import {
    campaignError
} from '../../store/actions';
import {
    isFetching,
    lastUpdated,
    errorMessage,
    campaignRecord,
    platformObject
} from '../../store/selectors/campaign.selectors';

class CampaignContainer extends PureComponent {

    handleErrorCloseClick = e => {
        e.preventDefault();
        this.props.onCampaignError();
    }

    render() {
        const {
            campaignRecord,
            platform,
            isFetching,
            lastUpdated, 
            errorMessage
        } = this.props;

        return (
            <div>
                {errorMessage !== null &&
                    <p className="error">
                        {errorMessage}&nbsp;
                        <button onClick={this.handleErrorCloseClick}>X</button>
                    </p>
                }
                <p>
                    {lastUpdated !== null  &&
                        <span>
                            Last updated at {new Date(lastUpdated).toLocaleTimeString()}.
                            {' '}
                        </span>
                    }
                </p>
                {isFetching && campaignRecord &&
                    <Loading />
                }
                {!isFetching && !campaignRecord &&
                    <h3>Empty</h3>
                }
                {campaignRecord && campaignRecord.platforms &&
                    <div style={{ opacity: isFetching ? 0.5 : 1 }}>
                        <PlatformTabsContainer />
                        {platform &&
                            <div>
                                <CampaignPlatformInsights insights={platform.insights} />
                                <CampaignPlatformOther platform={platform} />
                            </div>
                        }
                    </div>                    
                }
            </div>            
        );
    }
}

CampaignContainer.propTypes = {
    campaignRecord: PropTypes.object,
    platform: PropTypes.object,
    isFetching: PropTypes.bool,
    lastUpdated: PropTypes.number,
    errorMessage: PropTypes.string
};

function mapStateToProps(state) {

    return {
        campaignRecord: campaignRecord(state),
        platform: platformObject(state),
        isFetching: isFetching(state),
        lastUpdated: lastUpdated(state),
        errorMessage: errorMessage(state)
    }
}

function mapDispatchToProps(dispatch) {

    return {
        onCampaignError: () => dispatch(campaignError())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(CampaignContainer);
