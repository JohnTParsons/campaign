import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const formatDate = (millis) => (new Date(millis)).toLocaleDateString();

const formatList = (list) => ((!list || !list.length) ? '' : list.join(', '));

const formatRange = (range) => ((!range || range.length < 2) ? '' : range.join(' - '));

const displayImage = (image) => (<img width="400" src={`/images/${image}`} alt="Creatives"></img>);

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.primary,
    color: theme.palette.secondary,
  },
  body: {
    fontSize: 14
  },
}))(TableCell);

export default class CampaignPlatformOther extends PureComponent {

  render() {
    const { platform } = this.props;
    if (!platform) {
      return null;
    }
    const targetAudience = platform.target_audiance || {};
    const creatives = platform.creatives || {};

    return (
      <div>
        <Table>
          <TableBody>
            <TableRow>
              <CustomTableCell>Status</CustomTableCell>
              <TableCell>{platform.status}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Total Budget</CustomTableCell>
              <TableCell>{platform.total_budget}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Remaining Budget</CustomTableCell>
              <TableCell>{platform.remaining_budget}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Start Date</CustomTableCell>
              <TableCell>{formatDate(platform.start_date)}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>End Date</CustomTableCell>
              <TableCell>{formatDate(platform.end_date)}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Languages</CustomTableCell>
              <TableCell>{formatList(targetAudience.languages)}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Genders</CustomTableCell>
              <TableCell>{formatList(targetAudience.genders)}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Age Range</CustomTableCell>
              <TableCell>{formatRange(targetAudience.age_range)}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Locations</CustomTableCell>
              <TableCell>{formatList(targetAudience.locations)}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Interests</CustomTableCell>
              <TableCell>{formatList(targetAudience.interests)}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Header</CustomTableCell>
              <TableCell>{creatives.header}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Description</CustomTableCell>
              <TableCell>{creatives.description}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>URL</CustomTableCell>
              <TableCell>{creatives.url}</TableCell>
            </TableRow>
            <TableRow>
              <CustomTableCell>Image</CustomTableCell>
              <TableCell>{displayImage(creatives.image)}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

CampaignPlatformOther.propTypes = {
  platform: PropTypes.object.isRequired
};

CampaignPlatformOther.defaultProps = {
  platform: null
};
