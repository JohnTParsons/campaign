import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export default class CampaignPlatformInsights extends PureComponent {

  render() {
    const { insights } = this.props;
    if (!insights) {
      return null;
    }

    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <CustomTableCell>Impressions</CustomTableCell>
              <CustomTableCell>Clicks</CustomTableCell>
              <CustomTableCell>Visits</CustomTableCell>
              <CustomTableCell>Nanos Score</CustomTableCell>
              <CustomTableCell>Cost Per Click</CustomTableCell>
              <CustomTableCell>Clickthru</CustomTableCell>
              <CustomTableCell>Adv KPI 1</CustomTableCell>
              <CustomTableCell>Adv KPI 2</CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            <TableRow>
              <TableCell>{insights.impressions}</TableCell>
              <TableCell>{insights.clicks}</TableCell>
              <TableCell>{insights.website_visits}</TableCell>
              <TableCell>{insights.nanos_score}</TableCell>
              <TableCell>{insights.cost_per_click}</TableCell>
              <TableCell>{insights.click_through_rate}</TableCell>
              <TableCell>{insights.advanced_kpi_1}</TableCell>
              <TableCell>{insights.advanced_kpi_2}</TableCell>
            </TableRow>
          </TableBody>
        </Table>
      </div>
    );
  }
}

CampaignPlatformInsights.propTypes = {
  insights: PropTypes.object.isRequired
};

CampaignPlatformInsights.defaultProps = {
  insights: null
};
