import './Navigation.scss';

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { NavLink, withRouter } from 'react-router-dom';

class Navigation extends Component {

    render() {
        return (
           <NavLink
           to={{ pathname: this.props.path, state: { prevPath: this.props.location.pathname } }}
           exact={true}
           className="nav">{this.props.label}</NavLink>
        );
    }
}

Navigation.propTypes = {
  path: PropTypes.string.required,
  label: PropTypes.string.required
};

export default withRouter(Navigation);
