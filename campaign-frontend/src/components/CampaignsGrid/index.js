import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableHead from '@material-ui/core/TableHead';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableRow from '@material-ui/core/TableRow';

const CustomTableCell = withStyles(theme => ({
  head: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  body: {
    fontSize: 14,
  },
}))(TableCell);

export default class CampaignsGrid extends PureComponent {

  render() {
    const { records, selectedCampaign, onCampaignSelection } = this.props;

    const onRowSelection = (index) => onCampaignSelection({ index, id: records[index].id });

    return (
      <div>
        <Table>
          <TableHead>
            <TableRow>
              <CustomTableCell>ID</CustomTableCell>
              <CustomTableCell>Name</CustomTableCell>
              <CustomTableCell>Goals</CustomTableCell>
              <CustomTableCell numeric>Total Budget</CustomTableCell>
              <CustomTableCell>Status</CustomTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {records.map((campaign, index) => (
              <TableRow key={index} selected={index===selectedCampaign.index} onClick={() => onRowSelection(index)}>
                <TableCell>{campaign.id}</TableCell>
                <TableCell>{campaign.name}</TableCell>
                <TableCell>{campaign.goal}</TableCell>
                <TableCell numeric>{campaign.total_budget}</TableCell>
                <TableCell>{campaign.status}</TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </div>
    );
  }
}

CampaignsGrid.propTypes = {
  records: PropTypes.array.isRequired,
  onCampaignSelection: PropTypes.function,
  selectedCampaign: PropTypes.object
};

CampaignsGrid.defaultProps = {
  records: [],
  onCampaignSelection: () => {},
  selectedCampaign: { index: -1, id: -1 }
};
