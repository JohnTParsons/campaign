import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';
import Navigation from '../Navigation';

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

class Header extends React.Component {
  state = {
    auth: true,
    anchorElLeft: null
  };

  handleLeftMenuOpen = event => {
    this.setState({ anchorElLeft: event.currentTarget });
  };

  handleLeftMenuClose = () => {
    this.setState({ anchorElLeft: null });
  };

  render() {
    const { classes } = this.props;
    const { auth, anchorElLeft } = this.state;
    const openLeft = Boolean(anchorElLeft);

    return (
      <div className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton className={classes.menuButton} color="inherit" aria-label="Menu"
                        aria-owns="menu-left"
                        aria-haspopup="true"onClick={this.handleLeftMenuOpen}>
              <MenuIcon />
            </IconButton>
                        <Menu
              id="menu-left"
              anchorEl={anchorElLeft}
              open={openLeft}
              onClose={this.handleLeftMenuClose}
            >
              <MenuItem onClick={this.handleLeftMenuClose}><Navigation path="/" label="Home"/></MenuItem>
              <MenuItem onClick={this.handleLeftMenuClose}><Navigation path="/campaigns" label="Campaigns"/></MenuItem>
              <MenuItem onClick={this.handleLeftMenuClose}><Navigation path="/contacts" label="Contacts"/></MenuItem>
            </Menu>
            <Typography color="inherit" className={classes.grow}>
              Campaigns Dashboard
            </Typography>
            {auth && (
              <div>
                <IconButton
                  color="inherit"
                >
                  <AccountCircle />
                </IconButton>
              </div>
            )}
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Header);