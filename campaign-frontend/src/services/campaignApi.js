import axios from 'axios'

const graphqlUrl = 'http://localhost:4000/graphql';

export const getCampaigns = () => axios.post(graphqlUrl,
  {
    query: `
      query campaignsSummary {
        campaigns {
          id
          name
          goal
          total_budget
          status
        }
      }`
  }
).then((result) => {
  return result.data.data.campaigns;
});

export const getCampaign = (id) => axios.post(graphqlUrl,
  {
    query: `
      query campaignDetail {
        campaigns(id: ${id}) {
          id
          name
          goal
          total_budget
          status
          platforms {
            facebook {
              status
              total_budget
              remaining_budget
              start_date
              end_date
              target_audiance {
                languages
                genders
                age_range
                locations
                interests
              }
              creatives {
                header
                description
                url
                image
              }
              insights {
                impressions
                clicks
                website_visits
                nanos_score
                cost_per_click
                click_through_rate
                advanced_kpi_1
                advanced_kpi_2
              }
            }
            instagram {
              status
              total_budget
              remaining_budget
              start_date
              end_date
              target_audiance {
                languages
                genders
                age_range
                locations
                interests
              }
              creatives {
                header
                description
                url
                image
              }
              insights {
                impressions
                clicks
                website_visits
                nanos_score
                cost_per_click
                click_through_rate
                advanced_kpi_1
                advanced_kpi_2
              }
            }
            google {
              status
              total_budget
              remaining_budget
              start_date
              end_date
              target_audiance {
                languages
                genders
                age_range
                locations
                interests
              }
              creatives {
                header
                description
                url
                image
              }
              insights {
                impressions
                clicks
                website_visits
                nanos_score
                cost_per_click
                click_through_rate
                advanced_kpi_1
                advanced_kpi_2
              }
            }
          }
        }
      }`
  }
).then((result) => {
  return result.data.data.campaigns;
});
