import React, { Component } from 'react';

import CampaignsContainer from '../../containers/CampaignsContainer';
import CampaignContainer from '../../containers/CampaignContainer';

export default class Campaigns extends Component {

    render() {
        return (
            <div>
                <h2>Campaigns</h2>
                <br/>
                <CampaignsContainer />
                <br/>
                <CampaignContainer />
            </div>
        );
    }
}
