import Loadable from 'react-loadable';
import Loading from '../../components/Loading'

const LoadableCampaigns = Loadable({
    loader: () => import('./Campaigns'),
    loading: Loading
});

export const route = {
    path: '/campaigns',
    exact: true,
    label: 'Campaigns',
    component: LoadableCampaigns
};
