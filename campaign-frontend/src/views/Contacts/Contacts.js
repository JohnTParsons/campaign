import React, { PureComponent } from 'react';

export default class Contacts extends PureComponent {

    render() {
        return (
            <div>
                <h2>Contacts</h2>
                <ul>
                    <li>John Parsons</li>
                </ul>
            </div>
        );
    }
}
