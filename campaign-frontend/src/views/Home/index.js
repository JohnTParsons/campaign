import React, { Component } from 'react';

export default class Home extends Component {

    render() {
        return (
            <div>
                <h2>Home</h2>
                <p>
                    This is a demo React application for displaying advertising campaign information.
                </p>
                <p>
                  To use it, select Campaigns from the popup menu on the header. From the Campaigns list,
                  click on a row to see details about that campaign. You can see one Platform at a time
                  (out of Facebook, Instagram or Google).
                </p>
            </div>
        );
    }
}

export const route = {
    path: '/',
    exact: true,
    label: 'Home',
    component: Home
};
