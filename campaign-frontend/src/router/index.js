import { route as homeRoute } from '../views/Home';
import { route as campaignsRoute } from '../views/Campaigns';
import { route as contactsRoute } from '../views/Contacts';

export default [
    homeRoute,
    campaignsRoute,
    contactsRoute
];
