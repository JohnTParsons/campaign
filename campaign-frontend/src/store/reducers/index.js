import { reduce as campaignsReducer } from './campaigns.reducer';
import { reduce as campaignReducer } from './campaign.reducer.js';

export default {
    campaigns: campaignsReducer,
    campaign: campaignReducer
};
