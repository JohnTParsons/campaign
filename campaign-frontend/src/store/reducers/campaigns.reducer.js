import {
  CAMPAIGNS_REQUEST,
  CAMPAIGNS_RECEIVE,
  CAMPAIGNS_FAILURE,
  CAMPAIGNS_REQUEST_PAUSE,
  CAMPAIGNS_REQUEST_RESUME,
  CAMPAIGNS_ERROR,
  SELECT_CAMPAIGN
} from '../actions/campaigns.actions';

const initialState = {
    isFetching: false,
    records: [],
    receivedAt: null,
    pausePulling: false,
    errorMessage: null,
    selection: {index: -1, id:-1}
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {

        case CAMPAIGNS_REQUEST:
            return { ...state, isFetching: true, errorMessage: null };

        case CAMPAIGNS_RECEIVE:
            return {
                ...state,
                isFetching: false,
                records: action.campaigns,
                receivedAt: action.receivedAt,
                errorMessage: null
            };

        case CAMPAIGNS_FAILURE:
            return { ...state, isFetching: false, errorMessage: action.error.message };

        case CAMPAIGNS_REQUEST_PAUSE:
            return { ...state, isFetching: false, pausePulling: true };

        case CAMPAIGNS_REQUEST_RESUME:
            return { ...state, pausePulling: false };

        case CAMPAIGNS_ERROR:
            return { ...state, errorMessage: null };

        case SELECT_CAMPAIGN:
            return { ...state, selection: action.selection };

        default:
            return state;
    }
};
