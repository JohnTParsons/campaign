import {
    CAMPAIGN_REQUEST,
    CAMPAIGN_RECEIVE,
    CAMPAIGN_FAILURE,
    CAMPAIGN_ERROR,
    SELECT_PLATFORM
} from '../actions/campaign.actions';

const initialState = {
    isFetching: false,
    records: [],
    receivedAt: null,
    pausePulling: false,
    errorMessage: null,
    platformName: 'facebook'
};

export const reduce = (state = initialState, action = {}) => {

    switch (action.type) {
        
        case CAMPAIGN_REQUEST:
            return { ...state, isFetching: true, errorMessage: null };

        case CAMPAIGN_RECEIVE:
            return {
                ...state, 
                isFetching: false, 
                records: action.campaign,
                receivedAt: action.receivedAt, 
                errorMessage: null
            };

        case CAMPAIGN_FAILURE:
            return { ...state, isFetching: false, errorMessage: action.error.message };

        case CAMPAIGN_ERROR:
            return { ...state, errorMessage: null };

        case SELECT_PLATFORM:
            return { ...state, platformName: action.platformName };

        default:
            return state;
    }
};
