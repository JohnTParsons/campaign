export const CAMPAIGN_REQUEST = 'CAMPAIGN_REQUEST';
export const CAMPAIGN_RECEIVE = 'CAMPAIGN_RECEIVE';
export const CAMPAIGN_FAILURE = 'CAMPAIGN_FAILURE';
export const CAMPAIGN_REFRESH = 'CAMPAIGN_REFRESH';
export const CAMPAIGN_ERROR = 'CAMPAIGN_ERROR';
export const SELECT_PLATFORM = 'SELECT_PLATFORM';

function action(type, payload = {}) {
    return { type, ...payload }
}

export const requestCampaign = () => action(CAMPAIGN_REQUEST);
export const refreshCampaign = () => action(CAMPAIGN_REFRESH);
export const receiveCampaign = campaign => action(CAMPAIGN_RECEIVE, { campaign, receivedAt: Date.now() });
export const failureCampaign = (error) => action(CAMPAIGN_FAILURE, { error });
export const campaignError = () => action(CAMPAIGN_ERROR);

export const selectPlatform = (platformName) => action(SELECT_PLATFORM, { platformName });
