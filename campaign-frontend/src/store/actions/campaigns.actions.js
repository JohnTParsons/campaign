export const CAMPAIGNS_REQUEST = 'CAMPAIGNS_REQUEST';
export const CAMPAIGNS_RECEIVE = 'CAMPAIGNS_RECEIVE';
export const CAMPAIGNS_FAILURE = 'CAMPAIGNS_FAILURE';
export const CAMPAIGNS_REFRESH = 'CAMPAIGNS_REFRESH';
export const CAMPAIGNS_ERROR = 'CAMPAIGNS_ERROR';
export const CAMPAIGNS_REQUEST_PAUSE = 'CAMPAIGNS_REQUEST_PAUSE';
export const CAMPAIGNS_REQUEST_RESUME = 'CAMPAIGNS_REQUEST_RESUME';
export const SELECT_CAMPAIGN = 'SELECT_CAMPAIGN';

function action(type, payload = {}) {
    return { type, ...payload }
}

export const requestCampaigns = () => action(CAMPAIGNS_REQUEST);
export const refreshCampaigns = () => action(CAMPAIGNS_REFRESH);
export const receiveCampaigns = campaigns => action(CAMPAIGNS_RECEIVE, { campaigns, receivedAt: Date.now() });
export const failureCampaigns = (error) => action(CAMPAIGNS_FAILURE, { error });
export const campaignsError = () => action(CAMPAIGNS_ERROR);

export const doPauseCampaignsPulling = () => action(CAMPAIGNS_REQUEST_PAUSE);
export const doResumeCampaignsPulling = () => action(CAMPAIGNS_REQUEST_RESUME);

export const selectCampaign = (selection) => action(SELECT_CAMPAIGN, { selection });
