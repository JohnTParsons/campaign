import { put, fork, takeLatest } from 'redux-saga/effects';
import * as campaignsActions from '../actions/campaigns.actions';

function* onLocationChangeCampaigns(e, route) {

    if (e && e.payload && e.payload.pathname === route &&
      e.payload.state && e.payload.state.prevPath && e.payload.state.prevPath !== route) {

        yield put(campaignsActions.doResumeCampaignsPulling());
    } else {
        yield put(campaignsActions.doPauseCampaignsPulling());
    }
}

function onLocationChange(e) {

    onLocationChangeCampaigns(e, 'campaigns');
}

function* watchRouter() {
    yield takeLatest('@@router/LOCATION_CHANGE', onLocationChange);
}

// Default set of sagas
export default [
    fork(watchRouter)
]
