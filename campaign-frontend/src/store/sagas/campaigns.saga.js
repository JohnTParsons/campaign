import { call, put, fork, select, takeLatest } from 'redux-saga/effects';
import { delay } from 'redux-saga';
import * as actions from '../actions/campaigns.actions';
import * as selectors from '../selectors/campaigns.selectors';
import { getCampaigns } from '../../services';

function* fetchCampaignsSummary() {
    try {
        const campaigns = yield call(getCampaigns);
        return campaigns;
    } catch (error) {
        yield put(actions.failureCampaigns(error));
    }
}

function* updateResource() {
    const campaigns = yield call(fetchCampaignsSummary);
    const errorMessage = yield select(selectors.errorMessage);

    if (!errorMessage) {
        yield put(actions.receiveCampaigns(campaigns));
    }        
}

function* pullingCampaigns(a,b,c) {
    const pausePulling = yield select(selectors.pausePulling); 

    if (!pausePulling) {
        yield call(updateResource);
    }
    
    while (true) {
        // Auto-refresh once per minute
        yield call(delay, 60000);
        yield call(pullingCampaigns);
    }
}

function* startPulling() {
    yield put(actions.requestCampaigns());
}

function* watchCampaigns() {
    yield takeLatest(actions.CAMPAIGNS_REQUEST, pullingCampaigns);
    yield takeLatest(actions.CAMPAIGNS_REQUEST_RESUME, startPulling);
    yield takeLatest(actions.CAMPAIGNS_REFRESH, updateResource);
}

// Default set of sagas
export default [
    fork(watchCampaigns),
    fork(startPulling)
]
