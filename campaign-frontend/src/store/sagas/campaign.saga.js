import { call, put, fork, select, takeLatest } from 'redux-saga/effects';
import * as actions from '../actions';
import * as selectors from '../selectors/campaign.selectors';
import { getCampaign } from '../../services';

function* fetchCampaignDetails(id) {
    try {
        const campaign = yield call(() => getCampaign(id));
        return campaign;
    } catch (error) {
        yield put(actions.failureCampaign(error));
    }
}

function* updateResource(action) {
    const campaign = yield call(() => fetchCampaignDetails(action.selection.id));
    const errorMessage = yield select(selectors.errorMessage);

    if (!errorMessage) {
        yield put(actions.receiveCampaign(campaign));
    }        
}

function* watchCampaign() {
    yield takeLatest(actions.SELECT_CAMPAIGN, updateResource);
}

// Default set of sagas
export default [
    fork(watchCampaign)
]
