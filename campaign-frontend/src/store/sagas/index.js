import { all } from 'redux-saga/effects'
import rootRouterSagas from './router.saga';
import rootCampaignsSagas from './campaigns.saga';
import rootCampaignSagas from './campaign.saga';

export default function* rootSaga() {
    yield all([
        ...rootRouterSagas,
        ...rootCampaignsSagas,
        ...rootCampaignSagas
    ]);
}
