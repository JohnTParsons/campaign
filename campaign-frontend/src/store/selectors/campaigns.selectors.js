export const isFetching = state => !!state.campaigns.isFetching;
export const campaignRecordsRaw = state => state.campaigns.records || [];
export const lastUpdated = state => state.campaigns.receivedAt;
export const errorMessage = state => state.campaigns.errorMessage;
export const pausePulling = state => state.campaigns.pausePulling;
export const selectedCampaign = state => state.campaigns.selection;
