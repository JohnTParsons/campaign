import { createSelector } from 'reselect';

export const isFetching = state => !!state.campaign.isFetching;
export const campaignRecordsRaw = state => state.campaign.records || [];
export const platformName = state => state.campaign.platformName || 'facebook';
export const campaignRecord = createSelector(
  campaignRecordsRaw,
  records => records.length ? records[0] : null
);
export const platformObject = state => {
  const campaign = campaignRecord(state);
  const name = platformName(state);
  return campaign && platformName ? campaign.platforms[name] : null
};
export const lastUpdated = state => state.campaign.receivedAt;
export const errorMessage = state => state.campaign.errorMessage;
