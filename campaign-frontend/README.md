## Campaign Frontend

### Technologies
- React Router 4 
- Redux 
- Redux Saga 
- Persistent redux state 
- SASS for components styling 
- Code splitting 
- Material-UI

### Prerequisites
Globally install `yarn` for faster downloading of dependencies. Otherwise, enter `npm run` wherever yarn is mentioned, below.

In a new terminal, `cd campaign-frontend` to pick up commands defined in the frontend package.json

`yarn install` to install required node modules.

### Unit testing Web application
`yarn test` to start an interactive Jest testing session.

### Running Web application
`yarn start` to start the server running on local port 3000.

### Using Web application
Open `http://localhost:3000` in a browser.

Follow the on-screen instructions.
