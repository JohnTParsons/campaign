## Campaign Monorepo

This monorepo hosts two free-standing applications

### Campaign Backend
Follow the instructions in /campaign-backend/README

### Campaign Frontend
Follow the instructions in /campaign-frontend/README

### Technology choices
- Lokijs was chosen as the NoSQL as it small and fast, with a Mongo-like API and good support for importing from JSON
- GraphQL was chosen as the service transport as its ability to query for simple lists or big, nested objects make it a
good choice for 'master-detail' applications such as this one. For date timestamps, the GraphQL Int type wasn't long
enough (32-bit), so the Float type had the used to prevent runtime errors.
- Apollo 2 was chosen as the GraphQL Server for its combination of advanced features and easy setup
- Apollo 2 could have used as the GraphQL Client but, for this project, the Axios HTTP client was good enough
- Koa was chosen as the NodeJS server as it is smaller and more expressive than Express

This demo (https://github.com/kostysh/react-router-redux-saga-demo) was an influence as I liked its the project layout
and how it combined the core technologies.

### Possible future improvements
- Full Docker support and instructions
- Dotenv support to remove hardcoded ports
- GraphQL blackbox tests
- Frontend Storybook tests
- Integration tests
- More unit test coverage
