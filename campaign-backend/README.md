## Campaign Backend

### Technologies
- Koa
- Apollo GraphQL Server

### Prerequisites
Globally install `yarn` for faster downloading of dependencies. Otherwise, enter `npm run` wherever yarn is mentioned.

In a new terminal, `cd campaign-backend` to pick up commands defined in the backend package.json

`yarn install` to install required node modules.

### Unit testing GraphQL server
`yarn test` to start an interactive Jest testing session.

### Running GraphQL server
`yarn start` to start the server running on local port 4000.

### Smoke testing GraphQL server
Open `http://localhost:4000/graphql` in a browser.
The Apollo query 'playground' will appear, allowing you to drill-down through the schema and run queries.

(1) Check the server is running by entering and running this query:

```
{
  ping
}
```

It should return:

```
{
  "data": {
    "ping": true
  }
}
```

(2) Get campaign summary fields:

```
{
  campaigns {
    id
    name
    goal
    total_budget
    status
  }
}
```

It should return:

```
{
  "data": {
    "campaigns": [
      {
        "id": "100000001",
        "name": "Test Ad 1",
        "goal": "Increase Reach",
        "total_budget": 120,
        "status": "Delivering"
      },
      {
        "id": "100000002",
        "name": "Test Ad 2",
        "goal": "Raise Awareness",
        "total_budget": 360,
        "status": "Ended"
      },
      {
        "id": "100000003",
        "name": "Test Ad 3",
        "goal": "Raise Awareness",
        "total_budget": 90,
        "status": "Scheduled"
      }
    ]
  }
}
```
