export default (db, data) => {
  const campaigns = db.addCollection('campaigns');
  campaigns.insert(data);
  console.log('Campaigns loaded into NoSQL database');
  return campaigns;
};
