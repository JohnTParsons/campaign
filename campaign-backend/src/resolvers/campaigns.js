const arrayOrEmpty = campaign => campaign ? [campaign] : [];

export default (db, id) => {
  const campaignsDAO = db.getCollection('campaigns');
  // Find one or find all
  const campaigns = id ? arrayOrEmpty(campaignsDAO.findOne({ id })) : campaignsDAO.find({});
  return campaigns;
};
