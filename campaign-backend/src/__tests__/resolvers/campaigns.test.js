import Loki from 'lokijs';
import loadCampaigns from '../../database/load-campaigns';
import campaigns from '../../resolvers/campaigns';

describe('Campaigns Resolver', () => {
  const db = new Loki('test.db');
  const rawData = [{id: 1, name: 'Test 1'}, {id: 2, name: 'Test 2'}, {id: 3, name: 'Test 3'}];

  beforeAll(() => {
    loadCampaigns(db, rawData);
  });

  it('returns all campaigns when no id', () => {
    const lokiData = campaigns(db);
    expect(lokiData.length).toBe(rawData.length);
  });

  it('returns one campaign in array when matching id', () => {
    const lokiData = campaigns(db, 2);
    expect(lokiData.length).toBe(1);
    expect(lokiData[0]).toEqual(expect.objectContaining(rawData[1]));
  });

  it('returns no campaigns when id not found', () => {
    const lokiData = campaigns(db, 100);
    expect(lokiData.length).toBe(0);
  });
});
