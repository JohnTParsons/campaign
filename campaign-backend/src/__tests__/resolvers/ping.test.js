import ping from '../../resolvers/ping';

it('resolves to true', () => {
  expect(ping()).toBe(true);
});
