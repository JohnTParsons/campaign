import Loki from 'lokijs';
import loadCampaigns from '../../database/load-campaigns';

describe('Load Campaigns', () => {
  const db = new Loki('test.db');
  const rawData = [{id: 1, name: 'Test 1'}, {id: 2, name: 'Test 2'}, {id: 3, name: 'Test 3'}];
  let campaignsCollection;

  beforeAll(() => {
    campaignsCollection = loadCampaigns(db, rawData);
  });

  it('populates in-memory database', () => {
    expect(campaignsCollection.get(1)).toEqual(expect.objectContaining(rawData[0]));
    expect(campaignsCollection.get(2)).toEqual(expect.objectContaining(rawData[1]));
    expect(campaignsCollection.get(3)).toEqual(expect.objectContaining(rawData[2]));
  });
});
