import Koa from 'koa';
import { ApolloServer, gql } from 'apollo-server-koa';
import Loki from 'lokijs';

import loadCampaigns from '../database/load-campaigns';
import schema from '../schema/schema.graphql';
import resolvePing from '../resolvers/ping';
import resolveCampaigns from '../resolvers/campaigns';

const db = new Loki('loki.db');
const data = require('../../data/campaigns.json');

loadCampaigns(db, data);

// Constructs a schema based on GraphQL schema language
const typeDefs = gql`${schema}`;

// Resolver functions for the schema fields
const resolvers = {
  Query: {
    ping: () => resolvePing(),
    campaigns: (_, { id }) => resolveCampaigns(db, id)
  }
};

const server = new ApolloServer({ typeDefs, resolvers });

const app = new Koa();
server.applyMiddleware({ app });

app.listen({ port: 4000 }, () =>
  console.log(`GraphQL server ready at http://localhost:4000${server.graphqlPath}`)
);
